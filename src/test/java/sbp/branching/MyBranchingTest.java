package sbp.branching;

import com.sun.org.apache.xpath.internal.operations.String;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.OngoingStubbing;
import sbp.common.Utils;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.*;


public class MyBranchingTest {


    /**
     Тестируем метод maxInt из класса MyBranching
     при возврате значения true  методом utilFunc2()
     */
    @Test
    public void maxIntTestTrue() {
        final int i1 = 10;
        final int i2 = 5;
        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsForTest);

            int result = myBranching.maxInt(i1, i2);
            Assertions.assertEquals(0, result);
            System.out.print(result);
    }

    /**
     Тестируем метод maxInt из класса MyBranching
     при возврате значения false методом utilFunc2()
     */
    @Test
    public void maxIntTestFalse() {
        final int i1 = 15;
        final int i2 = 7;
        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsForTest);

        int result = myBranching.maxInt(i1, i2);
         if (result == 0) {
             Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);
             int result1 = myBranching.maxInt(i1, i2);
             System.out.print(result1);
         }
    }

    /**
     Тестируем метод ifElseExample из класса MyBranching
     при возврате значения true методом utilFunc2()
     */
    @Test
    public void  ifElseExample_True_Test() {

        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsForTest);

        boolean result = myBranching.ifElseExample();
        Assertions.assertTrue(result);

    }

    /**
     Тестируем метод ifElseExample из класса MyBranching
     при возврате значения false методом utilFunc2()
     */
    @Test
    public void  ifElseExample_False_Test() {

        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsForTest);

        boolean result = myBranching.ifElseExample();
        Assertions.assertFalse(result);
    }
    /**
     Тестируем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 0
     в методе switchExample из класса MyBranching
     */
    @Test
    public void switchExampleTest0(){

        final int i = 0;

        Utils funcTest = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(funcTest);
        myBranching.switchExample(i);

        Mockito.verify(funcTest, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(funcTest, Mockito.times(1)).utilFunc2();

        System.out.print("При i равном " + i + " utilFunc1 не выполняеися  и utilFunc2 выполняются один раз");
    }

    /**
     Тестируем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 1
     в методе switchExample из класса MyBranching
     */
    @Test
    public void switchExampleTest1(){
        final int i = 1;

        Utils funcTest = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(funcTest);
        myBranching.switchExample(i);

        Mockito.verify(funcTest, Mockito.times(1)).utilFunc1(anyString());
        Mockito.verify(funcTest, Mockito.times(1)).utilFunc2();

        System.out.print("При i равном " + i + " utilFunc1 и utilFunc2 выполняются по одному разу");
    }
    /**
     Тестируем выполнение Utils#utilFunc1 и Utils#utilFunc2 при входящем i = 2
     в методе switchExample из класса MyBranching
     */
    @Test
    public void switchExampleTest2(){
        final int i = 2;

        Utils funcTest = Mockito.mock(Utils.class);

        MyBranching myBranching = new MyBranching(funcTest);
        myBranching.switchExample(i);

        Mockito.verify(funcTest, Mockito.times(0)).utilFunc1(anyString());
        Mockito.verify(funcTest, Mockito.times(1)).utilFunc2();

        System.out.print("При i равном " + i + " utilFunc1 не выполняеися  и utilFunc2 выполняются один раз");
    }
}
