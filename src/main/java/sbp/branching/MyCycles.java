package sbp.branching;

import sbp.common.Utils;

public class MyCycles
{
    private final Utils utils;

    public MyCycles(Utils utils)
    {
        this.utils = utils;
    }

    /**
     * Необходимо написать реализацию метода с использованием for()
     +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     +     * Реализация Utils#utilFunc1() неизвестна
     +     * Должна присутствовать проверка возврщаемого значения от Utils#utilFunc1()
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     * @param iterations - количество итераций
     * @param str - строка для вывода через утилиту {@link Utils}
     */
    public void cycleForExample(int iterations, String str) {
        /**
        В цикле for вызывается Utils#utilFunc1() в каждой итерации
         проверяеися возвращение значения
         выводится сообщение о количестве итераций
         */
        int i;

        for (i = 0; i < iterations; i++) {

            final boolean b = this.utils.utilFunc1(str);

               if (b == true) {
                  System.out.println(str);
               }
        }
        System.out.print("Цыкл for выполняется " + i + " раз (раза) ");
    }

    /**
     * Необходимо написать реализацию метода с использованием While()
     +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     +     * Реализация Utils#utilFunc1() неизвестна
     +     * Должна присутствовать проверка возврщаемого значения от Utils#utilFunc1()
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     * @param iterations - количество итераций
     * @param str - строка для вывода через утилиту {@link Utils}
     */
    public void cycleWhileExample(int iterations, String str)
    {
        /**
         В цикле while вызывается Utils#utilFunc1() с проверкой
         возвращаемого значения
         */
        int i = 0;

        while (i < iterations) {

            final  boolean b = this.utils.utilFunc1(str);

            if (b == false) {

              System.out.println(str);
              break;
              } else {
                      System.out.println("utilFunc1 равен true");
                      break;
                     }
        }
    }

    /**
     * Необходимо написать реализацию метода с использованием while()
     -     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     -     * Реализация Utils#utilFunc1() неизвестна
     +     * Метод должен вызвать Utils#utilFunc1() на каждой итерации
     +     * Реализация Utils#utilFunc1() неизвестна
     * Должна присутствовать проверка возврщаемого значения
     * Результат проверяется через unit-test (все тесты должны выполниться успешно)
     * @param from - начальное значение итератора
     * @param to - конечное значение итератора
     * @param str - строка для вывода через утилиту {@link Utils}
     */
    public void cycleDoWhileExample(int from, int to, String str)
    {
        /**
        В цикле do while вызывается Utils#utilFunc1()
         при значении false выводит str
         */
        do {
            final  boolean b = this.utils.utilFunc1(str);

               if (b == false) {
                  System.out.print(str);
                  from++;
               } else {
                       System.out.println("utilFunc1 равен true");
                       break;
                      }
          } while (from < to);
           return;
    }
}
